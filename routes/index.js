var express = require('express');
var router = express.Router();
var http = require('http');

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.get('/fq', function(req, res) {

    var url = 'http://express.heartrails.com/api/json?method=getAreas';

    var response = http.get(url, function(apires){

        var body = '';
        apires.on('data', function(chunk){
            body += chunk;
        });

        apires.on('end', function(r){
            ret = JSON.parse(body);
            response = ret;
            console.log("Json: %j", response);

            res.json(response);

        });
    }).on('error', function(e){
        console.log(e.message);
    });

});

module.exports = router;
