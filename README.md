# nodejs install/nvm&npm&...

```bash
$ sudo yum check-update
$ sudo yum update -y
$ sudo yum install -y git openssl-devel
$ git clone https://github.com/creationix/nvm.git ~/.nvm
$ echo 'export PATH=$PATH:$HOME/.nvm/current/bin' >> ~/.bash_profile
$ echo 'source ~/.nvm/nvm.sh' >> ~/.bash_profile
$ source ~/.bash_profile
$ nvm ls-remote
$ nvm install v0.10.30
$ # nvm use v0.10
$ nvm ls
$ node # cli起動 Ctrl-c x2で抜ける
$ npm -v
```

# npm packages

```bash
$ npm install express-generator -g
$ npm install async
$ express -e qgenerator
```

# git clone

```bash
$ mkdir node && cd ndoe
$ git clone ssh://xxxxxxx
$ npm install
```

# start

```bash
$ npm start
```

# go
http://localhost:3000/
